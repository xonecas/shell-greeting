#!/bin/bash

# Depends on fortune, cowsay, lolcat and pfetch.
# Simply source this file from your ./bashrc or ./bash_profile

cows=(
  'www.cow'
  'vader.cow'
  'unipony-smaller.cow'
  'three-eyes.cow'
  'skeleton.cow'
  'sheep.cow'
  'moose.cow'
  'moofasa.cow'
  'flaming-sheep.cow'
  'default.cow'
  'cower.cow'
)

cows=( $(shuf -e "${cows[@]}") )
fortune | cowsay -f ${cows[0]} | lolcat -p 1.0 -F 0.06 -S $(echo $RANDOM)
PF_INFO="title os host kernel uptime memory" pfetch
